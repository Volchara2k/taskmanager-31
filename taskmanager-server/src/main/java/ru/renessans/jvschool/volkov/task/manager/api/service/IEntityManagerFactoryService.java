package ru.renessans.jvschool.volkov.task.manager.api.service;

import javax.persistence.EntityManagerFactory;

public interface IEntityManagerFactoryService {

    EntityManagerFactory buildFactory();

}