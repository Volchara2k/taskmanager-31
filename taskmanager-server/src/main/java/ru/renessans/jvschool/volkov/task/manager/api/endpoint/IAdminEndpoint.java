package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@SuppressWarnings("unused")
public interface IAdminEndpoint {

    @WebMethod
    @WebResult(name = "serverData", partName = "serverData")
    String serverData(
            SessionDTO sessionDTO
    );

    @WebMethod
    @WebResult(name = "closedAllSessions", partName = "closedAllSessions")
    boolean closedAllSessions(
            SessionDTO sessionDTO
    );

    @WebMethod
    @WebResult(name = "sessions", partName = "sessions")
    @NotNull
    Collection<SessionDTO> getAllSessions(
            @Nullable SessionDTO sessionDTO
    );

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    UserLimitedDTO signUpUserWithUserRole(
            @Nullable SessionDTO sessionDTO,
            @Nullable String login,
            @Nullable String password,
            @Nullable UserRole role
    );

    @WebMethod
    @WebResult(name = "deletedUser", partName = "deletedUser")
    @Nullable
    UserLimitedDTO deleteUserById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id
    );

    @WebMethod
    @WebResult(name = "deletedUser", partName = "deletedUser")
    @Nullable
    UserLimitedDTO deleteUserByLogin(
            @Nullable SessionDTO sessionDTO,
            @Nullable String login
    );

    @WebMethod
    @WebResult(name = "deletedUsers", partName = "deletedUsers")
    boolean deleteAllUsers(
            @Nullable SessionDTO sessionDTO
    );

    @WebMethod
    @WebResult(name = "lockedUser", partName = "lockedUser")
    @Nullable
    UserLimitedDTO lockUserByLogin(
            @Nullable SessionDTO sessionDTO,
            @Nullable String login
    );

    @WebMethod
    @WebResult(name = "unlockedUser", partName = "unlockedUser")
    @Nullable
    UserLimitedDTO unlockUserByLogin(
            @Nullable SessionDTO sessionDTO,
            @Nullable String login
    );

    @WebMethod
    @WebResult(name = "users", partName = "users")
    @Nullable
    Collection<UserLimitedDTO> setAllUsers(
            @Nullable SessionDTO sessionDTO,
            @Nullable Collection<UserLimitedDTO> usersDTO
    );

    @WebMethod
    @WebResult(name = "users", partName = "users")
    @NotNull
    Collection<UserLimitedDTO> getAllUsers(
            @Nullable SessionDTO sessionDTO
    );

    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @NotNull
    Collection<TaskDTO> getAllUsersTasks(
            @Nullable SessionDTO sessionDTO
    );

    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @Nullable
    Collection<TaskDTO> setAllUsersTasks(
            @Nullable SessionDTO sessionDTO,
            @Nullable Collection<TaskDTO> tasksDTO
    );

    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @NotNull
    Collection<ProjectDTO> getAllUsersProjects(
            @Nullable SessionDTO sessionDTO
    );

    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @Nullable
    Collection<ProjectDTO> setAllUsersProjects(
            @Nullable SessionDTO sessionDTO,
            @Nullable Collection<ProjectDTO> projectsDTO
    );

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    UserLimitedDTO getUserById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id
    );

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    UserLimitedDTO getUserByLogin(
            @Nullable SessionDTO sessionDTO,
            @Nullable String login
    );

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    UserLimitedDTO editProfileById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id,
            @Nullable String firstName
    );

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    UserLimitedDTO editProfileByIdWithLastName(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName
    );

    @WebMethod
    @WebResult(name = "updatedUser", partName = "updatedUser")
    @Nullable
    UserLimitedDTO updatePasswordById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id,
            @Nullable String newPassword
    );

}