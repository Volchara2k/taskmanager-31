package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.service.*;

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                DataInterChangeServiceTest.class,
                DomainServiceTest.class,
                ConfigurationServiceTest.class,
                SessionServiceTest.class,
        }
)

public abstract class AbstractServiceTestRunner {
}