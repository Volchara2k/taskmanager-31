package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.CommandRepository;

import java.util.Collection;

@Category(IntegrationImplementation.class)
public final class CommandServiceTest {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Test
    @Ignore
    @TestCaseName("Run testGetAllCommands for getAllCommands()")
    public void testGetAllCommands() {
        Assert.assertNotNull(this.commandService);
        @Nullable final Collection<AbstractCommand> allCommands = this.commandService.getAllCommands();
        Assert.assertNotNull(allCommands);
        Assert.assertNotEquals(0, allCommands.size());
    }

    @Test
    @Ignore
    @TestCaseName("Run testGetAllTerminalCommands for getAllTerminalCommands()")
    public void testGetAllTerminalCommands() {
        Assert.assertNotNull(this.commandService);
        @Nullable final Collection<AbstractCommand> allTerminalCommands = this.commandService.getAllTerminalCommands();
        Assert.assertNotNull(allTerminalCommands);
        Assert.assertNotEquals(0, allTerminalCommands.size());
    }

    @Test
    @Ignore
    @TestCaseName("Run testAllArgumentCommands for getAllArgumentCommands()")
    public void testAllArgumentCommands() {
        Assert.assertNotNull(this.commandService);
        @Nullable final Collection<AbstractCommand> allArgumentCommands = this.commandService.getAllArgumentCommands();
        Assert.assertNotNull(allArgumentCommands);
        Assert.assertNotEquals(0, allArgumentCommands.size());
    }

    @Test
    @Ignore
    @TestCaseName("Run testGetTerminalCommand for getTerminalCommand(command)")
    public void testGetTerminalCommand() {
        Assert.assertNotNull(this.commandService);
        @NotNull final String command = "help";
        Assert.assertNotNull(command);
        @Nullable final AbstractCommand terminalCommand = this.commandService.getTerminalCommand(command);
        Assert.assertNotNull(terminalCommand);
    }

    @Test
    @Ignore
    @TestCaseName("Run testGetArgumentCommand for getArgumentCommand(argument)")
    public void testGetArgumentCommand() {
        Assert.assertNotNull(this.commandService);
        @NotNull final String argument = "-h";
        Assert.assertNotNull(argument);
        @Nullable final AbstractCommand command = this.commandService.getArgumentCommand(argument);
        Assert.assertNotNull(command);
    }

}