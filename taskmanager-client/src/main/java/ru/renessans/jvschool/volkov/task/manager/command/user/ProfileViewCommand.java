package ru.renessans.jvschool.volkov.task.manager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class ProfileViewCommand extends AbstractUserCommand {

    @NotNull
    private static final String CMD_VIEW_PROFILE = "view-profile";

    @NotNull
    private static final String DESC_VIEW_PROFILE = "обновить пароль пользователя";

    @NotNull
    private static final String NOTIFY_VIEW_PROFILE =
            "Происходит попытка инициализации отображения информации о текущем пользователе \n" +
                    "Информация о текущем профиле пользователя: ";

    @Autowired
    public ProfileViewCommand(
            @NotNull final UserEndpoint userEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(userEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_VIEW_PROFILE;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_VIEW_PROFILE;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_VIEW_PROFILE);
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @Nullable final UserLimitedDTO view = super.userEndpoint.getUser(current);
        ViewUtil.print(view);
    }

}