package ru.renessans.jvschool.volkov.task.manager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class ProfileEditCommand extends AbstractUserCommand {

    @NotNull
    private static final String CMD_EDIT_PROFILE = "edit-profile";

    @NotNull
    private static final String DESC_EDIT_PROFILE = "изменить данные пользователя";

    @NotNull
    private static final String NOTIFY_EDIT_PROFILE =
            "Происходит попытка инициализации редактирования данных пользователя. \n" +
                    "Для обновления данных пользователя введите его имя или имя с фамилией. ";

    @Autowired
    public ProfileEditCommand(
            @NotNull final UserEndpoint userEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(userEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_EDIT_PROFILE;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_EDIT_PROFILE;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_EDIT_PROFILE);
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @NotNull final String firstName = ViewUtil.getLine();
        @Nullable final UserLimitedDTO edit = super.userEndpoint.editProfile(current, firstName);
        ViewUtil.print(edit);
    }

}