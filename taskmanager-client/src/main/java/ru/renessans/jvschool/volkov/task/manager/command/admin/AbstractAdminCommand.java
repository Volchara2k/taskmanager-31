package ru.renessans.jvschool.volkov.task.manager.command.admin;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminEndpoint;

public abstract class AbstractAdminCommand extends AbstractCommand {

    @NotNull
    protected final AdminEndpoint adminEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

    @Autowired
    protected AbstractAdminCommand(
            @NotNull final AdminEndpoint adminEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        this.adminEndpoint = adminEndpoint;
        this.currentSessionService = currentSessionService;
    }

}