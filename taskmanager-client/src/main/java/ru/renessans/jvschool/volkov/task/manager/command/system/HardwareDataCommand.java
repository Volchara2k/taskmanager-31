package ru.renessans.jvschool.volkov.task.manager.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.util.SystemUtil;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class HardwareDataCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_INFO = "info";

    @NotNull
    private static final String ARG_INFO = "-i";

    @NotNull
    private static final String DESC_INFO = "вывод информации о системе";

    @NotNull
    private static final String NOTIFY_INFO = "Информация о системе: \n";

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_INFO;
    }

    @NotNull
    @Override
    public String argument() {
        return ARG_INFO;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_INFO;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_INFO);
        ViewUtil.print(SystemUtil.getHardwareData());
    }

}