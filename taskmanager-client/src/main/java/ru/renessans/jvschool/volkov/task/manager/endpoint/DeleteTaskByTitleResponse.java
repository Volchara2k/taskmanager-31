package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteTaskByTitleResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="deleteTaskByTitleResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deletedTask" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}taskDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteTaskByTitleResponse", propOrder = {
        "deletedTask"
})
public class DeleteTaskByTitleResponse {

    protected TaskDTO deletedTask;

    /**
     * Gets the value of the deletedTask property.
     *
     * @return possible object is
     * {@link TaskDTO }
     */
    public TaskDTO getDeletedTask() {
        return deletedTask;
    }

    /**
     * Sets the value of the deletedTask property.
     *
     * @param value allowed object is
     *              {@link TaskDTO }
     */
    public void setDeletedTask(TaskDTO value) {
        this.deletedTask = value;
    }

}
