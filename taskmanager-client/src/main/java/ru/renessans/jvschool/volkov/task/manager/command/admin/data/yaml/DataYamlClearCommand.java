package ru.renessans.jvschool.volkov.task.manager.command.admin.data.yaml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.data.AbstractAdminDataCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class DataYamlClearCommand extends AbstractAdminDataCommand {

    @NotNull
    private static final String CMD_YAML_CLEAR = "data-yaml-clear";

    @NotNull
    private static final String DESC_YAML_CLEAR = "очистить yaml данные";

    @NotNull
    private static final String NOTIFY_YAML_CLEAR = "Происходит процесс очищения yaml данных...";

    @Autowired
    public DataYamlClearCommand(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_YAML_CLEAR;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_YAML_CLEAR;
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        final boolean clear = super.adminDataInterChangeEndpoint.dataYamlClear(current);
        ViewUtil.print(NOTIFY_YAML_CLEAR);
        ViewUtil.print(clear);
    }

}