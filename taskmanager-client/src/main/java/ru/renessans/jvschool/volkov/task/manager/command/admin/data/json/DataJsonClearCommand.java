package ru.renessans.jvschool.volkov.task.manager.command.admin.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.data.AbstractAdminDataCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class DataJsonClearCommand extends AbstractAdminDataCommand {

    @NotNull
    private static final String CMD_JSON_CLEAR = "data-json-clear";

    @NotNull
    private static final String DESC_JSON_CLEAR = "очистить json данные";

    @NotNull
    private static final String NOTIFY_JSON_CLEAR = "Происходит процесс очищения json данных...";

    @Autowired
    public DataJsonClearCommand(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_JSON_CLEAR;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_JSON_CLEAR;
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        final boolean close = super.adminDataInterChangeEndpoint.dataJsonClear(current);
        ViewUtil.print(NOTIFY_JSON_CLEAR);
        ViewUtil.print(close);
    }

}