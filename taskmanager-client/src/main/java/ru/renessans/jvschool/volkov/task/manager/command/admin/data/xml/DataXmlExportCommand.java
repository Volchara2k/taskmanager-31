package ru.renessans.jvschool.volkov.task.manager.command.admin.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.data.AbstractAdminDataCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class DataXmlExportCommand extends AbstractAdminDataCommand {

    @NotNull
    private static final String CMD_XML_EXPORT = "data-xml-export";

    @NotNull
    private static final String DESC_XML_EXPORT = "экспортировать домен в xml вид";

    @NotNull
    private static final String NOTIFY_XML_EXPORT = "Происходит процесс выгрузки домена в xml вид...";

    @Autowired
    public DataXmlExportCommand(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_XML_EXPORT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_XML_EXPORT;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @NotNull final DomainDTO exportData = super.adminDataInterChangeEndpoint.exportDataXml(current);
        ViewUtil.print(NOTIFY_XML_EXPORT);
        ViewUtil.print(exportData.getUsers());
        ViewUtil.print(exportData.getTasks());
        ViewUtil.print(exportData.getProjects());
    }

}