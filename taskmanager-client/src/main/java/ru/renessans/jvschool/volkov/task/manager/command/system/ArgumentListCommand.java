package ru.renessans.jvschool.volkov.task.manager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Collection;

@Component
@SuppressWarnings("unused")
public final class ArgumentListCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_ARGUMENTS = "arguments";

    @NotNull
    private static final String ARG_ARGUMENTS = "-arg";

    @NotNull
    private static final String DESC_ARGUMENTS = "вывод списка поддерживаемых программных аргументов";

    @NotNull
    private static final String NOTIFY_ARGUMENTS = "Список поддерживаемых программных аргументов: \n";

    @NotNull
    private final ICommandService commandService;

    @Autowired
    public ArgumentListCommand(@NotNull final ICommandService commandService) {
        this.commandService = commandService;
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_ARGUMENTS;
    }

    @NotNull
    @Override
    public String argument() {
        return ARG_ARGUMENTS;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_ARGUMENTS;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_ARGUMENTS);
        @Nullable final Collection<AbstractCommand> arguments = this.commandService.getAllArgumentCommands();
        ViewUtil.print(arguments);
    }

}