package ru.renessans.jvschool.volkov.task.manager.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

@Component
public abstract class AbstractCommand {

    @NotNull
    public abstract String terminalCommand();

    @Nullable
    public abstract String argument();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception;

    @NotNull
    @Override
    public String toString() {
        @NotNull final StringBuilder result = new StringBuilder();
        result.append("Терминальная команда: ").append(terminalCommand());
        if (ValidRuleUtil.isNotNullOrEmpty(argument()))
            result.append(", программный аргумент: ").append(argument());
        result.append("\n\t - ").append(description());
        return result.toString();
    }

}