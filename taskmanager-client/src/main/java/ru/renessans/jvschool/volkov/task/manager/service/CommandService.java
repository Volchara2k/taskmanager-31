package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.command.EmptyCommandException;

import java.util.Collection;
import java.util.Objects;

@Service
public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository iCommandRepository;

    @Lazy
    @Autowired
    public CommandService(
            @NotNull final ICommandRepository commandRepository
    ) {
        this.iCommandRepository = commandRepository;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllCommands() {
        return this.iCommandRepository.getAllCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllTerminalCommands() {
        return this.iCommandRepository.getAllTerminalCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllArgumentCommands() {
        return this.iCommandRepository.getAllArgumentCommands();
    }

    @Nullable
    @SneakyThrows
    @Override
    public AbstractCommand getTerminalCommand(
            @Nullable final String commandLine
    ) {
        if (Objects.isNull(commandLine)) throw new EmptyCommandException();
        return this.iCommandRepository.getTerminalCommand(commandLine);
    }

    @Nullable
    @SneakyThrows
    @Override
    public AbstractCommand getArgumentCommand(
            @Nullable final String commandLine
    ) {
        if (Objects.isNull(commandLine)) throw new EmptyCommandException();
        return this.iCommandRepository.getArgumentCommand(commandLine);
    }

}