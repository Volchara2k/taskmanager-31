package ru.renessans.jvschool.volkov.task.manager.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.AbstractAdminCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class UserLockCommand extends AbstractAdminCommand {

    @NotNull
    private static final String CMD_USER_LOCK = "user-lock";

    @NotNull
    private static final String DESC_USER_LOCK = "заблокировать пользователя (администратор)";

    @NotNull
    private static final String NOTIFY_USER_LOCK =
            "Происходит попытка инициализации блокирования пользователя. \n" +
                    "Для блокирования пользователя введите его логин. ";

    @Autowired
    public UserLockCommand(
            @NotNull final AdminEndpoint adminEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_USER_LOCK;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_USER_LOCK;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_USER_LOCK);
        @NotNull final String login = ViewUtil.getLine();
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @NotNull final UserLimitedDTO lock = super.adminEndpoint.lockUserByLogin(current, login);
        ViewUtil.print(lock);
    }

}