package ru.renessans.jvschool.volkov.task.manager.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.AbstractAdminCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class UserUnlockCommand extends AbstractAdminCommand {

    @NotNull
    private static final String CMD_USER_UNLOCK = "user-unlock";

    @NotNull
    private static final String DESC_USER_UNLOCK = "разблокировать пользователя (администратор)";

    @NotNull
    private static final String NOTIFY_USER_UNLOCK =
            "Происходит попытка инициализации разблокирования пользователя. \n" +
                    "Для разблокирования пользователя в системе введите его логин. ";

    @Autowired
    public UserUnlockCommand(
            @NotNull final AdminEndpoint adminEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_USER_UNLOCK;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_USER_UNLOCK;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_USER_UNLOCK);
        @NotNull final String login = ViewUtil.getLine();
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @NotNull final UserLimitedDTO unlock = super.adminEndpoint.unlockUserByLogin(current, login);
        ViewUtil.print(unlock);
    }

}