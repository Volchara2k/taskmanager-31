package ru.renessans.jvschool.volkov.task.manager.command.admin.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.data.AbstractAdminDataCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class DataBase64ImportCommand extends AbstractAdminDataCommand {

    @NotNull
    private static final String CMD_BASE64_IMPORT = "data-base64-import";

    @NotNull
    private static final String DESC_BASE64_IMPORT = "импортировать домен из base64 вида";

    @NotNull
    private static final String NOTIFY_BASE64_IMPORT = "Происходит процесс загрузки домена из base64 вида...";

    @Autowired
    public DataBase64ImportCommand(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_BASE64_IMPORT;
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public @NotNull String description() {
        return DESC_BASE64_IMPORT;
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @NotNull final DomainDTO importData = super.adminDataInterChangeEndpoint.importDataBase64(current);
        ViewUtil.print(NOTIFY_BASE64_IMPORT);
        ViewUtil.print(importData.getUsers());
        ViewUtil.print(importData.getTasks());
        ViewUtil.print(importData.getProjects());
    }

}