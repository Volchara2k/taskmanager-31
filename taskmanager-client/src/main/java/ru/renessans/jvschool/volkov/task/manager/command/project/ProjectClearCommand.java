package ru.renessans.jvschool.volkov.task.manager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Collection;

@Component
@SuppressWarnings("unused")
public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private static final String CMD_PROJECT_CLEAR = "project-clear";

    @NotNull
    private static final String DESC_PROJECT_CLEAR = "очистить все проекты";

    @NotNull
    private static final String NOTIFY_PROJECT_CLEAR = "Происходит попытка инициализации очистки списка проектов... ";

    @Autowired
    public ProjectClearCommand(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(projectEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_PROJECT_CLEAR;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_PROJECT_CLEAR;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_PROJECT_CLEAR);
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @Nullable final Collection<ProjectDTO> projects = super.projectEndpoint.deleteAllProjects(current);
        ViewUtil.print(projects);
    }

}