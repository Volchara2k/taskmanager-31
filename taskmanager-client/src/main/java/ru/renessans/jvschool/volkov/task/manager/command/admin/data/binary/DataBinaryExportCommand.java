package ru.renessans.jvschool.volkov.task.manager.command.admin.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.data.AbstractAdminDataCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class DataBinaryExportCommand extends AbstractAdminDataCommand {

    @NotNull
    private static final String CMD_BIN_EXPORT = "data-bin-export";

    @NotNull
    private static final String DESC_BIN_EXPORT = "экспортировать домен в бинарный вид";

    @NotNull
    private static final String NOTIFY_BIN_EXPORT = "Происходит процесс выгрузки домена в бинарный вид...";

    @Autowired
    public DataBinaryExportCommand(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_BIN_EXPORT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_BIN_EXPORT;
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @NotNull final DomainDTO exportData = super.adminDataInterChangeEndpoint.exportDataBin(current);
        ViewUtil.print(NOTIFY_BIN_EXPORT);
        ViewUtil.print(exportData.getUsers());
        ViewUtil.print(exportData.getTasks());
        ViewUtil.print(exportData.getProjects());
    }

}