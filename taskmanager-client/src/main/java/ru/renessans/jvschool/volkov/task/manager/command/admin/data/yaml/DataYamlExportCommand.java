package ru.renessans.jvschool.volkov.task.manager.command.admin.data.yaml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.data.AbstractAdminDataCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class DataYamlExportCommand extends AbstractAdminDataCommand {

    @NotNull
    private static final String CMD_YAML_EXPORT = "data-yaml-export";

    @NotNull
    private static final String DESC_YAML_EXPORT = "экспортировать домен в yaml вид";

    @NotNull
    private static final String NOTIFY_YAML_EXPORT =
            "Происходит попытка инициализации процесса выгрузки домена в yaml вид...";

    @Autowired
    public DataYamlExportCommand(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_YAML_EXPORT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_YAML_EXPORT;
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @NotNull final DomainDTO exportData = adminDataInterChangeEndpoint.exportDataYaml(current);
        ViewUtil.print(NOTIFY_YAML_EXPORT);
        ViewUtil.print(exportData.getUsers());
        ViewUtil.print(exportData.getTasks());
        ViewUtil.print(exportData.getProjects());
    }

}