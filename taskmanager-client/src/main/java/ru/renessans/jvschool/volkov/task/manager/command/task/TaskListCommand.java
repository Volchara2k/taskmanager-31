package ru.renessans.jvschool.volkov.task.manager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Collection;

@Component
@SuppressWarnings("unused")
public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    private static final String CMD_TASK_LIST = "task-list";

    @NotNull
    private static final String DESC_TASK_LIST = "вывод списка задач";

    @NotNull
    private static final String NOTIFY_TASK_LIST = "Текущий список задач: ";

    @Autowired
    public TaskListCommand(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(taskEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_TASK_LIST;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_TASK_LIST;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_TASK_LIST);
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @Nullable final Collection<TaskDTO> tasks = super.taskEndpoint.getAllTasks(current);
        ViewUtil.print(tasks);
    }

}