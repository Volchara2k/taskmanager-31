package ru.renessans.jvschool.volkov.task.manager.command.admin.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.data.AbstractAdminDataCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class DataBinaryClearCommand extends AbstractAdminDataCommand {

    @NotNull
    private static final String CMD_BIN_CLEAR = "data-bin-clear";

    @NotNull
    private static final String DESC_BIN_CLEAR = "очистить бинарные данные";

    @NotNull
    private static final String NOTIFY_BIN_CLEAR = "Происходит процесс очищения бинарных данных...";

    @Autowired
    public DataBinaryClearCommand(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_BIN_CLEAR;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_BIN_CLEAR;
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        final boolean clear = super.adminDataInterChangeEndpoint.dataBinClear(current);
        ViewUtil.print(NOTIFY_BIN_CLEAR);
        ViewUtil.print(clear);
    }

}