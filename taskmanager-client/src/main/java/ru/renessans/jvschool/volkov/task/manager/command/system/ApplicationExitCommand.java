package ru.renessans.jvschool.volkov.task.manager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Objects;

@Component
@SuppressWarnings("unused")
public final class ApplicationExitCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_EXIT = "exit";

    @NotNull
    private static final String DESC_EXIT = "закрыть приложение";

    @NotNull
    private static final String NOTIFY_EXIT = "Выход из приложения!";

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final ICurrentSessionService currentSessionService;

    @Autowired
    public ApplicationExitCommand(
            @NotNull final SessionEndpoint sessionEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        this.sessionEndpoint = sessionEndpoint;
        this.currentSessionService = currentSessionService;
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_EXIT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_EXIT;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_EXIT);
        @Nullable final SessionDTO current = this.currentSessionService.getSession();
        closeIfExists(current);
        System.exit(0);
    }

    private void closeIfExists(@Nullable final SessionDTO session) {
        if (Objects.isNull(session)) return;
        @Nullable final SessionDTO close = this.sessionEndpoint.closeSession(session);
        ViewUtil.print(close);
    }

}