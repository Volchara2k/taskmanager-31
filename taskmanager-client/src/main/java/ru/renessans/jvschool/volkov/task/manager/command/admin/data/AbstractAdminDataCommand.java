package ru.renessans.jvschool.volkov.task.manager.command.admin.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;

public abstract class AbstractAdminDataCommand extends AbstractCommand {

    @NotNull
    protected final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

    @Autowired
    protected AbstractAdminDataCommand(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        this.adminDataInterChangeEndpoint = adminDataInterChangeEndpoint;
        this.currentSessionService = currentSessionService;
    }

}