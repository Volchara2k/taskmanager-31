package ru.renessans.jvschool.volkov.task.manager.command.admin.data.yaml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.data.AbstractAdminDataCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class DataYamlImportCommand extends AbstractAdminDataCommand {

    @NotNull
    private static final String CMD_YAML_IMPORT = "data-yaml-import";

    @NotNull
    private static final String DESC_YAML_IMPORT = "импортировать домен из yaml вида";

    @NotNull
    private static final String NOTIFY_YAML_IMPORT = "Происходит процесс загрузки домена из yaml вида...";

    @Autowired
    public DataYamlImportCommand(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_YAML_IMPORT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_YAML_IMPORT;
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @NotNull final DomainDTO importData = super.adminDataInterChangeEndpoint.importDataYaml(current);
        ViewUtil.print(NOTIFY_YAML_IMPORT);
        ViewUtil.print(importData.getUsers());
        ViewUtil.print(importData.getTasks());
        ViewUtil.print(importData.getProjects());
    }

}