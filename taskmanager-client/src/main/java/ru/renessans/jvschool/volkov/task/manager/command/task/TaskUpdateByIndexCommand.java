package ru.renessans.jvschool.volkov.task.manager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";

    @NotNull
    private static final String DESC_TASK_UPDATE_BY_INDEX = "обновить задачу по индексу";

    @NotNull
    private static final String NOTIFY_TASK_UPDATE_BY_INDEX =
            "Происходит попытка инициализации обновления задачи \n" +
                    "Для обновления задачи по индексу введите индекс задачи из списка.\n" +
                    "Для обновления задачи введите её заголовок или заголовок с описанием. ";

    @Autowired
    public TaskUpdateByIndexCommand(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(taskEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_TASK_UPDATE_BY_INDEX;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_TASK_UPDATE_BY_INDEX;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_TASK_UPDATE_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();

        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @Nullable final TaskDTO update = super.taskEndpoint.updateTaskByIndex(current, index, title, description);
        ViewUtil.print(update);
    }

}