package ru.renessans.jvschool.volkov.task.manager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class TaskViewByTitleCommand extends AbstractTaskCommand {

    @NotNull
    private static final String CMD_TASK_VIEW_BY_TITLE = "task-view-by-title";

    @NotNull
    private static final String DESC_TASK_VIEW_BY_TITLE = "просмотреть задачу по заголовку";

    @NotNull
    private static final String NOTIFY_TASK_VIEW_BY_TITLE =
            "Происходит попытка инициализации отображения задачи. \n" +
                    "Для отображения задачи по заголовку введите заголовок задачи из списка. ";

    @Autowired
    public TaskViewByTitleCommand(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(taskEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_TASK_VIEW_BY_TITLE;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_TASK_VIEW_BY_TITLE;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_TASK_VIEW_BY_TITLE);
        @NotNull final String title = ViewUtil.getLine();
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @Nullable final TaskDTO view = super.taskEndpoint.getTaskByTitle(current, title);
        ViewUtil.print(view);
    }

}