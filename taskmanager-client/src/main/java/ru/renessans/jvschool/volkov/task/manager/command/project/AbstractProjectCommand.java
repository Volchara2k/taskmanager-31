package ru.renessans.jvschool.volkov.task.manager.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected final ProjectEndpoint projectEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

    @Autowired
    protected AbstractProjectCommand(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        this.projectEndpoint = projectEndpoint;
        this.currentSessionService = currentSessionService;
    }

}