package ru.renessans.jvschool.volkov.task.manager.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class IllegalIndexException extends AbstractException {

    @NotNull
    private static final String INDEX_ILLEGAL =
            "Ошибка! Значение \"%s\" параметра \"индекс\" не является целочисленным типом данных!\n";

    public IllegalIndexException(@NotNull final String message) {
        super(String.format(INDEX_ILLEGAL, message));
    }

}