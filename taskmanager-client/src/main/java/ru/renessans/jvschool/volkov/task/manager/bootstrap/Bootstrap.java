package ru.renessans.jvschool.volkov.task.manager.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.exception.unknown.UnknownCommandException;
import ru.renessans.jvschool.volkov.task.manager.util.ScannerUtil;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Objects;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private ICommandService commandService;

    public void run(@Nullable final String... arguments) {
        final boolean isEmptyArgs = ValidRuleUtil.isNullOrEmpty(arguments);
        if (isEmptyArgs) terminalCommandExecuteLoop();
        else argumentExecute(Objects.requireNonNull(arguments[0]));
    }

    @SuppressWarnings("InfiniteLoopStatement")
    private void terminalCommandExecuteLoop() {
        @NotNull String commandLine;
        while (true) {
            try {
                commandLine = ScannerUtil.getLine();
                @Nullable final AbstractCommand command = this.commandService.getTerminalCommand(commandLine);
                if (Objects.isNull(command)) throw new UnknownCommandException(commandLine);
                command.execute();
            } catch (@NotNull final Exception exception) {
                System.err.print(exception.getMessage() + "\n");
            }
        }
    }

    private void argumentExecute(@NotNull final String argument) {
        try {
            @Nullable final AbstractCommand command = this.commandService.getArgumentCommand(argument);
            if (Objects.isNull(command)) throw new UnknownCommandException(argument);
            command.execute();
        } catch (@NotNull final Exception exception) {
            System.err.print(exception.getMessage() + "\n");
        }
    }

}