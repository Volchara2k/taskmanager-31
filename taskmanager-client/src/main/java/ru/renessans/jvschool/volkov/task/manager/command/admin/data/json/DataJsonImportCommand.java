package ru.renessans.jvschool.volkov.task.manager.command.admin.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.data.AbstractAdminDataCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class DataJsonImportCommand extends AbstractAdminDataCommand {

    @NotNull
    private static final String CMD_JSON_IMPORT = "data-json-import";

    @NotNull
    private static final String DESC_JSON_IMPORT = "импортировать домен из json вида";

    @NotNull
    private static final String NOTIFY_JSON_IMPORT = "Происходит процесс загрузки домена из json вида...";

    @Autowired
    public DataJsonImportCommand(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_JSON_IMPORT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_JSON_IMPORT;
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @NotNull final DomainDTO importData = super.adminDataInterChangeEndpoint.importDataJson(current);
        ViewUtil.print(NOTIFY_JSON_IMPORT);
        ViewUtil.print(importData.getUsers());
        ViewUtil.print(importData.getTasks());
        ViewUtil.print(importData.getProjects());
    }

}