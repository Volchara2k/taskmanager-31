package ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyAbstractDTOException extends AbstractException {

    @NotNull
    private static final String EMPTY_OWNER =
            "Ошибка! Параметр \"сущность\" является null!\n";

    public EmptyAbstractDTOException() {
        super(EMPTY_OWNER);
    }

}