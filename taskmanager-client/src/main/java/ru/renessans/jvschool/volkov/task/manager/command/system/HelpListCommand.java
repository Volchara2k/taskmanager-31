package ru.renessans.jvschool.volkov.task.manager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Collection;

@Component
@SuppressWarnings("unused")
public final class HelpListCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_HELP = "help";

    @NotNull
    private static final String ARG_HELP = "-h";

    @NotNull
    private static final String DESC_HELP = "вывод списка команд";

    @NotNull
    private static final String NOTIFY_HELP = "Список команд";

    @NotNull
    private final ICommandService commandService;

    @Autowired
    public HelpListCommand(@NotNull final ICommandService commandService) {
        this.commandService = commandService;
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_HELP;
    }

    @NotNull
    @Override
    public String argument() {
        return ARG_HELP;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_HELP;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_HELP);
        @Nullable final Collection<AbstractCommand> commands = this.commandService.getAllCommands();
        ViewUtil.print(commands);
    }

}