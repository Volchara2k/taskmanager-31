package ru.renessans.jvschool.volkov.task.manager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class ProjectDeleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String CMD_PROJECT_DELETE_BY_INDEX = "project-delete-by-index";

    @NotNull
    private static final String DESC_PROJECT_DELETE_BY_INDEX = "удалить проект по индексу";

    @NotNull
    private static final String NOTIFY_PROJECT_DELETE_BY_INDEX =
            "Происходит попытка инициализации удаления проекта. \n" +
                    "Для удаления проекта по индексу введите индекс проекта из списка. ";

    @Autowired
    public ProjectDeleteByIndexCommand(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(projectEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_PROJECT_DELETE_BY_INDEX;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_PROJECT_DELETE_BY_INDEX;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_PROJECT_DELETE_BY_INDEX);
        @NotNull final Integer current = ViewUtil.getInteger() - 1;
        @Nullable final SessionDTO open = super.currentSessionService.getSession();
        @Nullable final ProjectDTO delete = super.projectEndpoint.deleteProjectByIndex(open, current);
        ViewUtil.print(delete);
    }

}