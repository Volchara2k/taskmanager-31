package ru.renessans.jvschool.volkov.task.manager.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserEndpoint;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected final UserEndpoint userEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

    @Autowired
    protected AbstractUserCommand(
            @NotNull final UserEndpoint userEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        this.userEndpoint = userEndpoint;
        this.currentSessionService = currentSessionService;
    }

}