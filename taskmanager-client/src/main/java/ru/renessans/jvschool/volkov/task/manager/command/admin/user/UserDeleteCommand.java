package ru.renessans.jvschool.volkov.task.manager.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.AbstractAdminCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class UserDeleteCommand extends AbstractAdminCommand {

    @NotNull
    private static final String CMD_USER_DELETE = "user-delete";

    @NotNull
    private static final String DESC_USER_DELETE = "удалить пользователя (администратор)";

    @NotNull
    private static final String NOTIFY_USER_DELETE =
            "Происходит попытка инициализации удаления пользователя системы. \n" +
                    "Для удаления пользователя введите его логин. ";

    @Autowired
    public UserDeleteCommand(
            @NotNull final AdminEndpoint adminEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_USER_DELETE;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_USER_DELETE;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_USER_DELETE);
        @NotNull final String login = ViewUtil.getLine();
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @NotNull final UserLimitedDTO delete = super.adminEndpoint.deleteUserByLogin(current, login);
        ViewUtil.print(delete);
    }

}