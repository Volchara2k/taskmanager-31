package ru.renessans.jvschool.volkov.task.manager.command.admin.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.data.AbstractAdminDataCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class DataBase64ExportCommand extends AbstractAdminDataCommand {

    @NotNull
    private static final String CMD_BASE64_EXPORT = "data-base64-export";

    @NotNull
    private static final String DESC_BASE64_EXPORT = "экспортировать домен в base64 вид";

    @NotNull
    private static final String NOTIFY_BASE64_EXPORT = "Происходит процесс выгрузки домена в base64 вид...";

    @Autowired
    public DataBase64ExportCommand(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_BASE64_EXPORT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_BASE64_EXPORT;
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @NotNull final DomainDTO exportDataBase64 = super.adminDataInterChangeEndpoint.exportDataBase64(current);
        ViewUtil.print(NOTIFY_BASE64_EXPORT);
        ViewUtil.print(exportDataBase64.getUsers());
        ViewUtil.print(exportDataBase64.getTasks());
        ViewUtil.print(exportDataBase64.getProjects());
    }

}
