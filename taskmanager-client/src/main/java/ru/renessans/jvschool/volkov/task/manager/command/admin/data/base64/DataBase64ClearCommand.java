package ru.renessans.jvschool.volkov.task.manager.command.admin.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.data.AbstractAdminDataCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class DataBase64ClearCommand extends AbstractAdminDataCommand {

    @NotNull
    private static final String CMD_BASE64_CLEAR = "data-base64-clear";

    @NotNull
    private static final String DESC_BASE64_CLEAR = "очистить base64 данные";

    @NotNull
    private static final String NOTIFY_BASE64_CLEAR = "Происходит процесс очищения base64 данных...";

    @Autowired
    public DataBase64ClearCommand(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_BASE64_CLEAR;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_BASE64_CLEAR;
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        final boolean clear = super.adminDataInterChangeEndpoint.dataBase64Clear(current);
        ViewUtil.print(NOTIFY_BASE64_CLEAR);
        ViewUtil.print(clear);
    }

}