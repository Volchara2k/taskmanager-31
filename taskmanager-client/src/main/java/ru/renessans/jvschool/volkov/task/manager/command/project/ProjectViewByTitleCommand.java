package ru.renessans.jvschool.volkov.task.manager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class ProjectViewByTitleCommand extends AbstractProjectCommand {

    @NotNull
    private static final String CMD_PROJECT_VIEW_BY_TITLE = "project-view-by-title";

    @NotNull
    private static final String DESC_PROJECT_VIEW_BY_TITLE = "просмотреть проект по заголовку";

    @NotNull
    private static final String NOTIFY_PROJECT_VIEW_BY_TITLE =
            "Происходит попытка инициализации отображения проекта. \n" +
                    "Для отображения проекта по заголовку введите заголовок проекта из списка. ";

    @Autowired
    public ProjectViewByTitleCommand(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(projectEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_PROJECT_VIEW_BY_TITLE;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_PROJECT_VIEW_BY_TITLE;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_PROJECT_VIEW_BY_TITLE);
        @NotNull final String title = ViewUtil.getLine();
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @Nullable final ProjectDTO view = super.projectEndpoint.getProjectByTitle(current, title);
        ViewUtil.print(view);
    }

}