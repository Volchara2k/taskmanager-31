package ru.renessans.jvschool.volkov.task.manager.command.admin.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.AbstractAdminCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class ServerAboutCommand extends AbstractAdminCommand {

    @NotNull
    private static final String CMD_SERVER_ABOUT = "server-about";

    @NotNull
    private static final String DESC_SERVER_ABOUT = "получить информацию о сервере (администратор)";

    @NotNull
    private static final String NOTIFY_SERVER_ABOUT =
            "Происходит попытка инициализации просмотра информации о сервере системы...";

    @Autowired
    public ServerAboutCommand(
            @NotNull final AdminEndpoint adminEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_SERVER_ABOUT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_SERVER_ABOUT;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_SERVER_ABOUT);
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @NotNull final String serverAbout = super.adminEndpoint.serverData(current);
        ViewUtil.print(serverAbout);
    }

}