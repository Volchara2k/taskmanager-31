package ru.renessans.jvschool.volkov.task.manager.command.security;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionEndpoint;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class UserLogOutCommand extends AbstractSecurityCommand {

    @NotNull
    private static final String CMD_LOG_OUT = "log-out";

    @Nullable
    private static final String DESC_LOG_OUT = "выйти из системы";

    @NotNull
    private static final String NOTIFY_LOG_OUT = "Производится выход пользователя из системы...";

    @Autowired
    public UserLogOutCommand(
            @NotNull final SessionEndpoint sessionEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(sessionEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_LOG_OUT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_LOG_OUT;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_LOG_OUT);
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @Nullable final SessionDTO close = super.sessionEndpoint.closeSession(current);
        @NotNull final SessionDTO unsubscribe = super.currentSessionService.unsubscribe();
        ViewUtil.print(unsubscribe);
    }

}