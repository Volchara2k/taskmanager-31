package ru.renessans.jvschool.volkov.task.manager.command.admin.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.admin.data.AbstractAdminDataCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public final class DataXmlClearCommand extends AbstractAdminDataCommand {

    @NotNull
    private static final String CMD_XML_CLEAR = "data-xml-clear";

    @NotNull
    private static final String DESC_XML_CLEAR = "очистить xml данные";

    @NotNull
    private static final String NOTIFY_XML_CLEAR = "Происходит процесс очищения xml данных...";

    @Autowired
    public DataXmlClearCommand(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_XML_CLEAR;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_XML_CLEAR;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        final boolean clear = super.adminDataInterChangeEndpoint.dataXmlClear(current);
        ViewUtil.print(NOTIFY_XML_CLEAR);
        ViewUtil.print(clear);
    }

}