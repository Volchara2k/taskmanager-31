package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    @Nullable
    Collection<AbstractCommand> getAllCommands();

    @Nullable
    Collection<AbstractCommand> getAllTerminalCommands();

    @Nullable
    Collection<AbstractCommand> getAllArgumentCommands();

    @Nullable
    AbstractCommand getTerminalCommand(
            @Nullable String commandLine
    );

    @Nullable
    AbstractCommand getArgumentCommand(
            @Nullable String commandLine
    );

}