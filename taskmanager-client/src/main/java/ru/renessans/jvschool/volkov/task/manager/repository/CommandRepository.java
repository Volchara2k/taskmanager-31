package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public final class CommandRepository implements ICommandRepository {

    @NotNull
    @Autowired
    private Collection<AbstractCommand> commandList;

    @NotNull
    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> argumentMap = new LinkedHashMap<>();

    @PostConstruct
    @Override
    public void fillCommands() {
        getAllTerminalCommands().forEach(command -> commandMap.put(command.terminalCommand(), command));
        getAllArgumentCommands().forEach(command -> argumentMap.put(command.argument(), command));
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllCommands() {
        return commandList;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllTerminalCommands() {
        return commandList
                .stream()
                .filter(command -> ValidRuleUtil.isNotNullOrEmpty(command.terminalCommand()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllArgumentCommands() {
        return commandList
                .stream()
                .filter(command -> ValidRuleUtil.isNotNullOrEmpty(command.argument()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public AbstractCommand getTerminalCommand(@NotNull final String commandLine) {
        return this.commandMap.values()
                .stream()
                .filter(command -> commandLine.equals(command.terminalCommand()))
                .findAny()
                .orElse(null);
    }

    @Nullable
    @Override
    public AbstractCommand getArgumentCommand(@NotNull final String commandLine) {
        return this.argumentMap.values()
                .stream()
                .filter(command -> commandLine.equals(command.argument()))
                .findAny()
                .orElse(null);
    }

}