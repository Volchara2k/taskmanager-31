package ru.renessans.jvschool.volkov.task.manager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Collection;

@Component
@SuppressWarnings("unused")
public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    private static final String CMD_PROJECT_LIST = "project-list";

    @NotNull
    private static final String DESC_PROJECT_LIST = "вывод списка проектов";

    @NotNull
    private static final String NOTIFY_PROJECT_LIST = "Текущий список проектов: ";

    @Autowired
    public ProjectListCommand(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(projectEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String terminalCommand() {
        return CMD_PROJECT_LIST;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_PROJECT_LIST;
    }

    @Override
    public void execute() {
        ViewUtil.print(NOTIFY_PROJECT_LIST);
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @Nullable final Collection<ProjectDTO> projects = super.projectEndpoint.getAllProjects(current);
        ViewUtil.print(projects);
    }

}