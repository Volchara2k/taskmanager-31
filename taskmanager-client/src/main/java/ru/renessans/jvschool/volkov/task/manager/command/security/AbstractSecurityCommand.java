package ru.renessans.jvschool.volkov.task.manager.command.security;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionEndpoint;

public abstract class AbstractSecurityCommand extends AbstractCommand {

    @NotNull
    protected final SessionEndpoint sessionEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

    @Autowired
    protected AbstractSecurityCommand(
            @NotNull final SessionEndpoint sessionEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        this.sessionEndpoint = sessionEndpoint;
        this.currentSessionService = currentSessionService;
    }

}